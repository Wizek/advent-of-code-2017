{-# language RankNTypes #-}
{-# language ViewPatterns #-}
{-# language ConstraintKinds #-}
{-# language ExtendedDefaultRules #-}
{-# language PartialTypeSignatures #-}
{-# language NoMonomorphismRestriction #-}

{-# options_ghc -Wno-partial-type-signatures #-}

import Prelude hiding ((!!))
import Data.Map
import GHC.Stack
import Test.Hspec
import ComposeLTR
import Data.Maybe as May
import Data.Monoid
import Control.Lens hiding ((.>), Index)
import Data.Function
import qualified Data.Map as Map
import qualified Data.List as L

main = do
  -- print $ fromList (let l = [0..3e6] in zip l l) ! 2000000
  hspec $ do
    it "cabDistance" $ do
      cabDistance    1 ==? 0
      cabDistance   12 ==? 3
      cabDistance   23 ==? 2
      cabDistance 1024 ==? 31

    it "getCoord" $ do
      getCoord 1 ==? (0, 0)
      getCoord 2 ==? (1, 0)
      getCoord 3 ==? (1, 1)
      getCoord 4 ==? (0, 1)

    it "buildMap" $ do
      (buildMap 1 ^._1) ==? (1 =: (0, 0))
      (buildMap 2 ^._1) ==? (1 =: (0, 0) <> 2 =: (1, 0))
      (buildMap 2 ^._1) ==? (1 =: (0, 0) <> 2 =: (1, 0))

    it "nextMove" $ do
      let test = nextMove . vMapToMMap
      (test $ 1 =: (0, 0) <> 2 =: (1, 0) ) ==? (1, 1)
      (test $ 1 =: (0, 0) <> 2 =: (1, 0) <> 3 =: (1, 1) ) ==? (0, 1)
      (test $ 1 =: (0, 0) <> 2 =: (1, 0) <> 3 =: (1, 1)
        <> 4 =: (0, 1)) ==? (-1, 1)
      (test $ 1 =: (0, 0) <> 2 =: (1, 0) <> 3 =: (1, 1)
        <> 4 =: (0, 1) <> 5 =: (-1, 1)) ==? (-1, 0)

    it "direction" $ do
      (direction $ 1 =: (0, 0) <> 2 =: (1, 0)) ==? (1, 0)
      (direction $ 1 =: (0, 0) <> 2 =: (0, 1)) ==? (0, 1)
      (direction $ 3 =: (4, 5) <> 4 =: (4, 6)) ==? (0, 1)
      (direction $ 3 =: (4, 5) <> 4 =: (4, 4)) ==? (0,-1)

    it "leftTurn" $ do
      leftTurn ( 1, 0) ==? ( 0, 1)
      leftTurn ( 0, 1) ==? (-1, 0)
      leftTurn (-1, 0) ==? ( 0,-1)
      leftTurn ( 0,-1) ==? ( 1, 0)

    it "leftTurn" $ do
      valueFor 1 ==? 1
      valueFor 2 ==? 1
      valueFor 3 ==? 2
      valueFor 4 ==? 4
      valueFor 5 ==? 5


  buildMap 100
    & view _3
    & toList
    & L.map snd
    & dropWhile (<= ourInput)
    & head
    & print

  print $ cabDistance ourInput

type MemoryMap = (IndexMap, CoordMap, ValueMap)
type IndexMap = Map Index Coord
type ValueMap = Map Index Value
type CoordMap = Map Coord Index
type Value = Int
type Index = Int
type Coord = (X, Y)
type Vector = (X, Y)
type X = Int
type Y = Int

ourInput = 361527

cabDistance :: Int -> Int
cabDistance = getCoord .> \(x, y) -> abs x + abs y

getCoord :: Int -> Coord
getCoord = buildMap .> view _1 .> findMax .> view _2


valueFor :: Index -> Value
valueFor = buildMap .> view _3 .> findMax .> view _2


vMapToMMap :: IndexMap -> MemoryMap
vMapToMMap vMap = (vMap, vMap & toList & L.map swap & fromList, mempty)

swap (a, b) = (b, a)

buildMap :: Int -> MemoryMap
buildMap n | n <= 1 = addMMap 1 (0, 0) 1 (mempty, mempty, mempty)
buildMap 2          = addMMap 2 (1, 0) 1 (buildMap 1)
buildMap n          = addMMap n currentMove currentValue prev
  where
  currentValue :: Value
  currentValue = sorroundingCoords currentMove
    & May.mapMaybe ((prevCoordMap !?))
    & May.mapMaybe ((prevValueMap !?))
    & sum
  currentMove = nextMove prev
  prev = buildMap prevScore
  prevCoordMap = prev ^._2
  prevValueMap = prev ^._3
  prevScore = n - 1
  prevCoord = view _1 prev ! prevScore


sorroundingCoords :: Coord -> [Coord]
sorroundingCoords c = L.map (vAdd c) sorroundingDirections

sorroundingDirections :: [Vector]
sorroundingDirections =
  let vs = [1,0,-1] in (,) <$> vs <*> vs $> L.filter (/=(0,0))

addMMap :: Index -> Coord -> Value -> MemoryMap -> MemoryMap
addMMap i c v (im, cm, vm) = (im <> i =: c, cm <> c =: i, vm <> i =: v)

direction :: IndexMap -> Vector
direction (takeMaxN 2 .> toList .> L.map snd -> [p, pp]) = pp `vSub` p

vSub :: Coord -> Coord -> Vector
vSub (x1, y1) (x2, y2) = (x1 - x2, y1 - y2)

vAdd :: Vector -> Vector -> Coord
vAdd (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

leftTurn :: Vector -> Vector
leftTurn ( 1, 0) = ( 0, 1)
leftTurn ( 0, 1) = (-1, 0)
leftTurn (-1, 0) = ( 0,-1)
leftTurn ( 0,-1) = ( 1, 0)

nextMove :: MemoryMap -> Coord
nextMove (vm, cm, _)
  | (last `vAdd` left) `notMember` cm = last `vAdd` left
  | otherwise                         = last `vAdd` straight
  where
  last = lastPos vm
  straight = direction vm
  left = leftTurn straight

lastPos :: IndexMap -> Coord
lastPos = findMax .> snd

u = undefined

takeMaxN :: Ord k => Int -> Map k v -> Map k v
takeMaxN n _ | n <= 0 = mempty
takeMaxN n m = Map.maxViewWithKey m $> maybe mempty
  (\((k, v), m2) -> k =: v <> takeMaxN (n - 1) m2)

(=:) = Map.singleton
-- infixl 1 =:

type HCS = HasCallStack
(==?) :: (HCS, _) => _
(==?) = shouldBe

{-
  17  16  15  14  13
  18   5   4   3  12
  19   6   1   2  11
  20   7   8   9  10
  21  22  23---> ...
-}
