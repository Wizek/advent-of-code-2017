{-# language RankNTypes #-}
{-# language ConstraintKinds #-}
-- {-# language OverloadedLists #-}
{-# language FlexibleContexts #-}
{-# language PatternSynonyms  #-}
{-# language TemplateHaskell  #-}
{-# language QuasiQuotes  #-}
{-# language ViewPatterns  #-}
{-# language GeneralizedNewtypeDeriving #-}
{-# language TupleSections  #-}
{-# language ExtendedDefaultRules #-}
{-# language ScopedTypeVariables #-}
{-# language OverloadedStrings #-}
{-# language TypeApplications #-}
{-# language PartialTypeSignatures #-}
{-# language NoMonomorphismRestriction #-}

{-# options_ghc -Wno-partial-type-signatures #-}

import            Prelude                hiding (lookup)
import qualified  Prelude                as P
import            Data.List              (find)
import qualified  Data.List              as L
import qualified  Data.Map               as M
import qualified  Data.Set               as S
import            Test.Hspec
import            Data.Maybe             --(isNothing, fromJust, isJust)
import            ComposeLTR
import            Data.IntMap            hiding (map)
import qualified  Data.IntMap            as IM
import            Control.Lens           hiding ((.>), Index, (+=), re)
import            Data.Function
import            Control.Arrow
import            Data.Ord
import            Data.Text              (Text)
import qualified  Data.Text              as T
import qualified  Data.Text.IO              as T
import qualified  Data.Foldable          as F
import            Control.Applicative
import            Control.Monad
import            Data.IORef
import            Data.Char
import            Debug.Trace
import            Data.Void
import            Data.Monoid
import            Data.List.Unique
import            Data.String.Conversions
import            Text.RE
import            Text.Megaparsec
import            Text.Megaparsec.Text
import            Text.RE.Replace
import            Text.RE.TDFA.Text
import            Text.InterpolatedString.Perl6
import            Debug.Pretty.Simple
import            Text.Pretty.Simple

import            Data.Coerce

default (Int, (), Text, String, [])

type Label = Text

streamP = nestedGroupP

nestedGroupP :: Int -> Parser (Int, GarbageLength)
nestedGroupP i = do
  gl0 <- opFillerP
  char '{'
  gl1 <- opFillerP
  r <- fmap (fromMaybe (0, 0)) $ optional $ nestedGroupP (i + 1)
  char '}'
  gl2 <- opFillerP
  r2 <- many $ nestedGroupP i

  pure (fst r + sum (map fst r2) + i + 1, gl0 + gl1 + gl2 + snd r + sum (map (snd) r2))

opFillerP = optional fillterP <&> fromMaybe 0

fillterP :: Parser GarbageLength
fillterP = fmap sum $ many $ (char ',' >> pure 0) <|> garbageP

garbageP :: Parser GarbageLength
garbageP = do
  char '<'
  garbageContent

garbageContent :: Parser GarbageLength
garbageContent = do
  (char '>' >> pure 0) <|> do
    n <- escapedP <|> (anyChar >> pure 1)
    fmap (n+) garbageContent

escapedP :: Parser GarbageLength
escapedP = do
  char '!'
  anyChar
  pure $ 0

newtype GarbageLength = GL Int deriving (Show, Ord, Num, Eq)

parseAndCount :: (HCS, _) => Text -> (Int, GarbageLength)
parseAndCount inp = parse (streamP 0) "fff" inp $> either errHandler id
-- parseAndCount inp = parse streamP "fff"  inp $> either (\a-> error $ show $ pTraceShow (1,2) a) id
-- parseAndCount inp = parse streamP "fff"  inp $> either (pTraceId .> show .> error) id
  where
  errHandler = \a-> error $
    (parseErrorPretty $ trace (cs $ pShow (a)) a)
    <> "\nInp: "
    <> cs inp
    <> "\n"

main = do
  input <- T.readFile "day-9/input"
  -- input <- T.readFile "day-9/input-cleaner-3.txt"
  testInp <- T.readFile "day-8/testInput"

  input & parseAndCount & print

  hspec $ do
    specify "part 1" $ do
      let
        test :: (HCS, _) => _
        test a b = (parseAndCount a & fst) ==? b

      test "{}                       " 1
      test "{{{}}}                   " 6

      test "{{}}                     " 3
      test "{},{}                    " 2
      test "{{},{}}                  " 5


      test "{{{},{},{{}}}}           " 16
      test "{<{},{},{{}}>}           " 1
      test "{<a>,<a>,<a>,<a>}        " 1
      test "{{<a>},{<a>},{<a>},{<a>}}" 9
      test "{{<ab>},{<ab>},{<ab>},{<ab>}}" 9
      test "{{<!!>},{<!!>},{<!!>},{<!!>}}" 9
      test "{{<a!>},{<a!>},{<a!>},{<ab>}}" 3

      -- test "{{<!>},{<!>},{<!>},{<a>}}" 2

    specify "part 2" $ do
      let
        test :: (HCS, _) => _
        test a b = (parseAndCount a & snd) ==? b

      test "{<>}                 " 0
      test "{<random characters>}" 17
      test "{<<<<>}              " 3
      test "{<{!>}>}             " 2
      test "{<!!>}               " 0
      test "{<!!!>>}             " 0
      test "{<{o'i!a,<{i<a>}     " 10


data CShow a = CShow (a -> String) a
instance Show (CShow a) where
  show (CShow f a) = f a

tapTraceShow a = traceShow a a

sg :: Ord a => [a] -> [[a]]
sg = L.group . L.sort

sgBy :: Ord b => (a -> b) -> [a] -> [[a]]
sgBy fn = L.groupBy (\a b -> fn a == fn b) . L.sortOn fn

filterByLength :: Ord a => (Int -> Bool) -> [a] -> [[a]]
filterByLength p = L.filter (p . length) . sg

filterByLengthBy :: Ord b => (a -> b) -> (Int -> Bool) -> [a] -> [[a]]
filterByLengthBy fn p = L.filter (p . length) . sgBy fn

uniqueBy :: Ord b => (a -> b) -> [a] -> [a]
uniqueBy fn = concat . filterByLengthBy fn (==1)


pattern J a = Just a
pattern N   = Nothing

iterMayN advance n s = iterate (>>= advance) (Just s) !! n
iterN advance n s = iterate advance s !! n
fl = zip [0..] .> fromList

noop :: Monad m => m ()
noop = return ()

undef = undefined

sub = subtract

safePrint = id .> show .> take 1000 .> putStrLn

(=:) = M.singleton

type HCS = HasCallStack
(==?) :: (HCS, _) => _
(==?) = shouldBe


{-
  bash partial solutions

  ➜  advent-of-code git:(master) cat day-9/input | sed 's/!.//g' | sed 's/!.//g' | perl -pe 's|<.*?>||g' | sed 's/,//g' > day-9/input-cleaner-3.txt

  ➜  advent-of-code git:(master) cat day-9/input | sed 's/!.//g' | sed 's/!./x/g' | perl -pe 's|<(.*?)>||g' | wc -c
  2715
  ➜  advent-of-code git:(master) cat day-9/input | sed 's/!.//g' | sed 's/!./x/g' | perl -pe 's|<(.*?)>|$1|g' | wc -c
  7045
-}
