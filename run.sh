#!/usr/bin/env bash
# #!/usr/bin/env zsh

set -e

selection="5"

# nixRun='ghcid -c "ghci day-'$selection'/Main.hs" -T ":main --color -o ghcid-output.txt" -o ghcid-output.txt -S'
# nixRun='ghc day-'$selection'/Main.hs -O2 -threaded -fforce-recomp'
nixRun='ghc day-'$selection'/Main.hs -O2 -threaded -prof -fforce-recomp'

# nixRun='ghc day-'$selection'/Main.hs -O2 -threaded -prof -fforce-recomp -fexternal-interpreter'

# nixRun='ghc day-'$selection'/Main.hs -O2 -threaded -fforce-recomp -dynamic'
# nixRun='ghc day-'$selection'/Main.hs -O2 -threaded -fforce-recomp -prof -fprof-auto -osuf p_o'

packages=""
packages="$packages interpolatedstring-perl6"
packages="$packages text"
packages="$packages hspec"
packages="$packages lens"
packages="$packages compose-ltr"
packages="$packages non-empty"
packages="$packages pretty-simple"
packages="$packages Unique"
packages="$packages megaparsec"
packages="$packages (hl.doJailbreak regex)"
# packages="$packages (let a = (hl.doJailbreak regex-with-pcre); in builtins.trace (builtins.attrNames a) a)"
packages="$packages string-conversions"


compVer='ghc821'
# compVer='ghc822'



mayProfiled='(haskell.packages.'$compVer'.extend (self: super: {mkDerivation = expr: super.mkDerivation (expr // { enableLibraryProfiling = true; doCheck=false; doCoverage=false; doHaddock=false; configureFlags = (expr.configureFlags or []) ++ ["-fprof-auto"]; });}))'
# mayProfiled='(haskell.packages.'$compVer'.extend (self: super: {regex-pcre-text = (hl.doJailbreak super.regex-pcre-text); mkDerivation = expr: super.mkDerivation (expr // { enableLibraryProfiling = true; });}))'
# mayProfiled='haskell.packages.'$compVer''
# mayProfiled='haskell.packages.'$compVer''

p='let hl = haskell.lib; in ('"$mayProfiled"'.ghcWithPackages) (p: with p; ['"$packages"'])'

echo $p

nix-shell -p "$p" -j4 --run "$nixRun"

