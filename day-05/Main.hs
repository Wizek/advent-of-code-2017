{-# language RankNTypes #-}
{-# language ConstraintKinds #-}
{-# language OverloadedLists #-}
{-# language FlexibleContexts #-}
{-# language ExtendedDefaultRules #-}
{-# language PartialTypeSignatures #-}
{-# language NoMonomorphismRestriction #-}

{-# options_ghc -Wno-partial-type-signatures #-}

import            Prelude                hiding (lookup)
import qualified  Prelude                as P
import            Data.List              (find)
import            Test.Hspec
import            Data.Maybe             (isNothing, fromJust, isJust)
import            ComposeLTR
import            Data.IntMap
import            Control.Lens           hiding ((.>))
import            Data.Functor
import            Data.Function
import            Control.Arrow
import            Control.Applicative

default (Int, ())

type InsState = (Position, IntMap JumpCount)
type Position = Int
type JumpCount = Int

main = do
  hspec $ do
    let initialState = (0, fl [0, 3, 0, 1, -3])

    specify "advance 1" $ do
      iterMayN advance1 0 initialState ==? Just (0, fl [0, 3, 0, 1, -3])
      iterMayN advance1 1 initialState ==? Just (0, fl [1, 3, 0, 1, -3])
      iterMayN advance1 2 initialState ==? Just (1, fl [2, 3, 0, 1, -3])
      iterMayN advance1 3 initialState ==? Just (4, fl [2, 4, 0, 1, -3])
      iterMayN advance1 4 initialState ==? Just (1, fl [2, 4, 0, 1, -2])
      iterMayN advance1 5 initialState ==? Just (5, fl [2, 5, 0, 1, -2])

      iterMayN advance1 6 initialState ==? Nothing
      iterMayN advance1 7 initialState ==? Nothing

    specify "advance 2" $ do
      iterMayN advance2 10 initialState ==? Just (5, fl [2, 3, 2, 3, -1])
      iterMayN advance2 11 initialState ==? Nothing

  readFile "day-5/input" >>= process advance1 .> print
  readFile "day-5/input" >>= process advance2 .> print

process advance = words .> P.map read .> (\l -> (0, fl l))
  .> (\s -> zip [0..] $ iterate (>>= advance) (Just s))
  .> takeWhile (snd.>isJust) .> last .> fst

advance1 :: InsState -> Maybe InsState
advance1 (p, s) = lookup p s <&> \v -> (p + v, adjust (+1) p s)

advance2 :: InsState -> Maybe InsState
advance2 (p, s) = lookup p s <&> \v -> (p + v, adjust (if v >= 3 then (sub 1) else (+1)) p s)

sub = subtract
iterMayN advance n s = iterate (>>= advance) (Just s) !! n
fl = zip [0..] .> fromList
type HCS = HasCallStack
(==?) :: (HCS, _) => _
(==?) = shouldBe
