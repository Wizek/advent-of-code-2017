{-# language RankNTypes #-}
{-# language ConstraintKinds #-}
-- {-# language OverloadedLists #-}
{-# language FlexibleContexts #-}
{-# language PatternSynonyms  #-}
{-# language TemplateHaskell  #-}
{-# language QuasiQuotes  #-}
{-# language ViewPatterns  #-}
{-# language TupleSections  #-}
{-# language ExtendedDefaultRules #-}
{-# language ScopedTypeVariables #-}
{-# language OverloadedStrings #-}
{-# language TypeApplications #-}
{-# language PartialTypeSignatures #-}
{-# language NoMonomorphismRestriction #-}

{-# options_ghc -Wno-partial-type-signatures #-}

import            Prelude                hiding (lookup)
import qualified  Prelude                as P
import            Data.List              (find)
import qualified  Data.List              as L
import qualified  Data.Map               as M
import qualified  Data.Set               as S
import            Test.Hspec
import            Data.Maybe             --(isNothing, fromJust, isJust)
import            ComposeLTR
import            Data.IntMap            hiding (map)
import qualified  Data.IntMap            as IM
import            Control.Lens           hiding ((.>), Index, (+=), re)
import            Data.Function
import            Control.Arrow
import            Data.Ord
import            Data.Text              (Text)
import qualified  Data.Text              as T
import qualified  Data.Foldable          as F
import            Control.Applicative
import            Control.Monad
import            Data.IORef
import            Data.Char
import            Debug.Trace
import            Data.Void
import            Data.Monoid
import            Data.List.Unique
import            Data.String.Conversions
import            Text.RE
import            Text.RE.Replace
import            Text.RE.PCRE.Text
import            Text.InterpolatedString.Perl6

default (Int, (), Text, String, [])

data Ins = Ins Label Int Label Operator Int deriving (Show, Eq)
data Operator = Olt | Ogt | Oeq | Oleq | Ogeq | Oneq
  deriving (Show, Eq)
type Label = Text


test1 = [qq|b inc 5 if a > 1
a inc 1 if b < 5
c dec -10 if a >= 1
c inc -20 if c == 10|]

main = do
  input <- readFile "day-8/input"
  testInp <- readFile "day-8/testInput"

  print test1

  hspec $ do
    specify "process" $ do
      process1 "aa inc 1 if bb > -1\naa inc 1 if bb > -1"  ==? ("aa" =: 2)

    specify "process" $ do
      process1 test1 ==? ("c" =: (-10) <> "a" =: 1)

    specify "part 1" $ do
      -- run1 "ga dec -169 if znx > 1570" mempty ==? mempty
      run1 "aa inc 1 if bb > 1" M.empty ==? mempty
      run1 "aa inc 1 if bb > 1" ("bb" =: 2) ==? ("bb" =: 2 <> "aa" =: 1)

      run1 "a inc 1 if b < 5" mempty ==? ("a" =: 1)
      run1 "c dec -10 if a >= 1" ("a" =: 1) ==? ("a" =: 1 <> "c" =: 10)
      process1 "a inc 1 if b < 5\nc dec -10 if a >= 1" ==? ("a" =: 1 <> "c" =: 10)


    specify "parse" $ do
      parse1 "aa inc 1 if bb > 1" ==? Ins "aa" ( 1) "bb" Ogt 1
      parse1 "aa dec 1 if bb > 1" ==? Ins "aa" (-1) "bb" Ogt 1
      parse1 "aa inc -1 if bb > 1" ==? Ins "aa" (-1) "bb" Ogt 1
      parse1 "aa dec -1 if bb > 1" ==? Ins "aa" ( 1) "bb" Ogt 1
      parse1 "aa dec -1 if cc == 1" ==? Ins "aa" ( 1) "cc" Oeq 1
      parse1 "aa dec -1 if cc < -3" ==? Ins "aa" ( 1) "cc" Olt (-3)


  let
    v1 = testInp
      & cs
      & process1

  -- v1
  --   & M.toList
  --   & L.maximumBy (comparing snd)
  --   & safePrint
  v1 & safePrint

  let
    v1 = input
      & cs
      & process1
  -- v1
  --   & M.toList
  --   & L.maximumBy (comparing snd)
  --   & safePrint
  v1 & safePrint

type State8 = M.Map Label Int
type State8WithMax = (Int, State8)

process1 :: Text -> State8
process1 = id
  .> T.lines
  .> map parse1
  .> L.foldl (flip exec) (0, M.empty)
  .> snd

run1  a = (0,) .> run a .> snd
exec1 a = (0,) .> run a .> snd

run :: Text -> State8WithMax -> State8WithMax
run ins s = exec (parse1 ins) s

exec :: Ins -> State8WithMax -> State8WithMax
exec (Ins name offset condName condOp condVal) s =
  if regCVal <=!> condVal
    then s & _2 %~ M.insert name (newVal)
           & _1 %~ (max newVal)
    else s
  where
  newVal = regVal+offset
  regVal = M.lookup name (snd s) $> fromMaybe 0
  regCVal = M.lookup condName (snd s) $> fromMaybe 0
  (<=!>) = opF condOp


parse1 :: Text -> Ins
parse1 str = Ins name (value * mul) condName op condVal
  where
  (_:name:sign:(r->value):condName:opT:(r->condVal):_)
    = P.map capturedText $ F.toList $ matchArray $ str
    =~ [re|^([a-z]+) ([a-z]+) ([-0-9]+) if ([a-z]+) ([!><=]+) ([-0-9]+)|]
  mul = case sign of
    "inc" -> ( 1)
    "dec" -> (-1)
    a     -> error $ cs a
  op = case opT of
    ">"  -> Ogt
    ">=" -> Ogeq
    "<"  -> Olt
    "<=" -> Oleq
    "==" -> Oeq
    "!=" -> Oneq
    a    -> error $ cs a

r :: Text -> Int
r = cs .> read @Int

opF Ogt  = (>)
opF Olt  = (<)
opF Oeq  = (==)
opF Oleq = (<=)
opF Ogeq = (>=)
opF Oneq = (/=)



tapTraceShow a = traceShow a a








sg :: Ord a => [a] -> [[a]]
sg = L.group . L.sort

sgBy :: Ord b => (a -> b) -> [a] -> [[a]]
sgBy fn = L.groupBy (\a b -> fn a == fn b) . L.sortOn fn

filterByLength :: Ord a => (Int -> Bool) -> [a] -> [[a]]
filterByLength p = L.filter (p . length) . sg

filterByLengthBy :: Ord b => (a -> b) -> (Int -> Bool) -> [a] -> [[a]]
filterByLengthBy fn p = L.filter (p . length) . sgBy fn

uniqueBy :: Ord b => (a -> b) -> [a] -> [a]
uniqueBy fn = concat . filterByLengthBy fn (==1)


pattern J a = Just a
pattern N   = Nothing

iterMayN advance n s = iterate (>>= advance) (Just s) !! n
iterN advance n s = iterate advance s !! n
fl = zip [0..] .> fromList

noop :: Monad m => m ()
noop = return ()

undef = undefined

sub = subtract

safePrint = id .> show .> take 1000 .> putStrLn

(=:) = M.singleton

type HCS = HasCallStack
(==?) :: (HCS, _) => _
(==?) = shouldBe
