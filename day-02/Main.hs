{-# language RankNTypes #-}
{-# language TupleSections #-}
{-# language ExtendedDefaultRules #-}
{-# language NoMonomorphismRestriction #-}

import Test.Hspec
import ComposeLTR
import Data.Function
import qualified Data.Set as S
-- import Data.List.NonEmpty
import Data.List (find, genericLength)
import Data.Maybe


main = do
  hspec $ do
    specify "uniqueCombinations" $ do
      uniqueCombinations [1,2] ==? [(1,2)]
      uniqueCombinations [1,2,3] ==? [(1,2), (1,3), (2,3)]
      uniqueCombinations [1] ==? []
      uniqueCombinations [] ==? []

    specify "divisible" $ do
      divisible 4 2 ==? True
      divisible 2 4 ==? False

  -- input & map (maximum <^(-)^> minimum) & sum & print
  readFile "day-2/input.tsv" >>= process1 .> print
  readFile "day-2/input.tsv" >>= process2 .> print

process1 = lines .> map (words .> map read) .> map (maximum <^(-)^> minimum) .> sum
process2 = lines .> map (words .> map read) .> map (id
  .> uniqueCombinations .> map swap .> find (uncurry divisible) .> fromJust
  .> (uncurry div)) .> sum

swap (a, b) = (b, a)
divisible a b = ((a `div` b) * b) == a

-- findDivs =
uniqueCombinations l = map (\i -> map (\ii -> (min i ii, max i ii)) l ) l $> concat
  $> S.fromList $> S.toList $> filterOut (uncurry (==))

filterKeep = filter
filterOut f = filterKeep (f .> not)

(<^) = flip (<$>)
(^>) = (<*>)
infixl 3 <^
infixl 3 ^>

(==?) = shouldBe

