{nixpkgs ? import <nixpkgs> {}}:
let
ps = packages
in nixpkgs.haskell.packages.ghc821.ghcWithPackages (p: with p; [])
