import Data.List
import ComposeLTR
import Data.Function
import Control.Arrow

main = readFile "./day-4/input" >>= (process1 &&& process2) .> print
process1 = lines .> map words .> filter (\l-> nub l == l) .> length
process2 = lines .> map words .> filter (map sort .> \l-> nub l == l) .> length
