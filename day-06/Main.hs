{-# language RankNTypes #-}
{-# language ConstraintKinds #-}
{-# language OverloadedLists #-}
{-# language FlexibleContexts #-}
{-# language PatternSynonyms  #-}
{-# language ExtendedDefaultRules #-}
{-# language PartialTypeSignatures #-}
{-# language NoMonomorphismRestriction #-}

{-# options_ghc -Wno-partial-type-signatures #-}

import            Prelude                hiding (lookup)
import qualified  Prelude                as P
import            Data.List              (find)
import qualified  Data.List              as L
import qualified  Data.Map               as M
import qualified  Data.Set               as S
import            Test.Hspec
import            Data.Maybe             (isNothing, fromJust, isJust)
import            ComposeLTR
import            Data.IntMap
import qualified  Data.IntMap            as IM
import            Control.Lens           hiding ((.>), Index, (+=))
import            Data.Function
import            Control.Arrow
import            Data.Ord
import            Control.Applicative
import            Control.Monad
import            Data.IORef

default (Int, (), String, [])

main = do
  hspec $ do
    specify "part 1" $ do
      iterN advance1 0 (fl [0, 2, 7, 0]) ==? fl [0, 2, 7, 0]
      iterN advance1 1 (fl [0, 2, 7, 0]) ==? fl [2, 4, 1, 2]
      iterN advance1 2 (fl [0, 2, 7, 0]) ==? fl [3, 1, 2, 3]
      iterN advance1 3 (fl [0, 2, 7, 0]) ==? fl [0, 2, 3, 4]

    specify "distribute" $ do
      distribute 0 0 (fl [0, 0, 0, 0]) ==? fl [0, 0, 0, 0]
      distribute 0 1 (fl [0, 0, 0, 0]) ==? fl [1, 0, 0, 0]
      distribute 0 2 (fl [0, 0, 0, 0]) ==? fl [1, 1, 0, 0]
      distribute 0 3 (fl [0, 0, 0, 0]) ==? fl [1, 1, 1, 0]
      distribute 0 4 (fl [0, 0, 0, 0]) ==? fl [1, 1, 1, 1]
      distribute 0 5 (fl [0, 0, 0, 0]) ==? fl [2, 1, 1, 1]

    specify "process1" $ do
      process1 (fl [0, 2, 7, 0]) ==? J 5

    specify "part 2" $ do
      process2 (fl [0, 2, 7, 0]) ==? J 4

  process1 (fl [0, 2, 7, 0]) & safePrint
  process1 input & safePrint
  process2 input & safePrint


process1 = processF .> fmap fst
process2 = processF .> fmap (uncurry (-))

processF :: BanksState  -> Maybe (CycleCount, CycleCount)
processF state = iterate advance1 state & zip [0..] & phi mempty
  where
  phi :: M.Map BanksState CycleCount -> [(CycleCount, BanksState)]
      -> Maybe (CycleCount, CycleCount)
  phi seen ((i, bs):rest) | i > 10000 = N
  phi seen ((i, bs):rest) = case bs `M.lookup` seen of
    J ii -> J (i, ii)
    N    -> phi (M.insert bs i seen) rest

type BanksState = IntMap BankVal
type BankVal = Int
type Index = Int
type CycleCount = Int

distribute _ n s | n <= 0      = s
distribute i n s | i >= size s = distribute 0 n s
distribute i n s = s
  & ix i %~ (+1)
  & distribute (i + 1) (n - 1)

advance1 :: BanksState -> BanksState
advance1 banks = banks
  & ix (mx^._1) .~ 0
  & distribute (fst mx + 1) (snd mx)
  where
  mx = banks $> toList $> reverse $> L.maximumBy (comparing snd)

input = fl [5, 1, 10, 0, 1, 7, 13, 14, 3, 12, 8, 10, 7, 12, 0, 6]

pattern J a = Just a
pattern N   = Nothing

iterMayN advance n s = iterate (>>= advance) (Just s) !! n
iterN advance n s = iterate advance s !! n
fl = zip [0..] .> fromList

noop :: Monad m => m ()
noop = return ()

sub = subtract

safePrint = id .> show .> take 1000 .> putStrLn

type HCS = HasCallStack
(==?) :: (HCS, _) => _
(==?) = shouldBe
