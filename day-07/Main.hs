{-# language RankNTypes #-}
{-# language ConstraintKinds #-}
-- {-# language OverloadedLists #-}
{-# language FlexibleContexts #-}
{-# language PatternSynonyms  #-}
{-# language ExtendedDefaultRules #-}
{-# language PartialTypeSignatures #-}
{-# language NoMonomorphismRestriction #-}

{-# options_ghc -Wno-partial-type-signatures #-}

import            Prelude                hiding (lookup)
import qualified  Prelude                as P
import            Data.List              (find)
import qualified  Data.List              as L
import qualified  Data.Map               as M
import qualified  Data.Set               as S
import            Test.Hspec
import            Data.Maybe             --(isNothing, fromJust, isJust)
import            ComposeLTR
import            Data.IntMap            hiding (map)
import qualified  Data.IntMap            as IM
import            Control.Lens           hiding ((.>), Index, (+=), re)
import            Data.Function
import            Control.Arrow
import            Data.Ord
import            Data.Text              (Text)
import qualified  Data.Text              as T
import            Control.Applicative
import            Control.Monad
import            Data.IORef
import            Data.Char
import            Data.Void
import            Data.Monoid
import            Data.List.Unique
import            Data.String.Conversions
import            Text.RE.TDFA.Text

default (Int, (), Text, String, [])

main = do
  testInp <- readFile "day-7/input"
  testInp
    & words
    & L.map (L.filter (isAlpha))
    & unique
    & print


  hspec $ do

    specify "weight" $ do
      weight "ab" ("ab" =: (1, [])) ==? 1
      weight "ab" ("ab" =: (1, []) <> "cd" =: (1, [])) ==? 1
      weight "ab" ("ab" =: (1, ["cd"]) <> "cd" =: (1, [])) ==? 2
      weight "ab" ("ab" =: (1, ["cd"]) <> "cd" =: (1, [])) ==? 2

    specify "isImbalanced" $ do
      isImbalanced "a" ("a" =: (1, ["b","c"]) <> "b" =: (1, []) <> "c" =: (1, []))
        ==? N
      isImbalanced "a" ("a" =: (1, ["b","c","d"]) <> "b" =: (1, [])
        <> "c" =: (2, []) <> "d" =: (1, [])) ==? J ("c", 2)
      isImbalanced "a" ("a" =: (1, ["b","c","d"]) <> "b" =: (1, [])
        <> "c" =: (2, []) <> "d" =: (2, [])) ==? J ("b", 1)
      isImbalanced "a" ("a" =: (1, ["b","c"]) <> "b" =: (1, [])
        <> "c" =: (2, [])) ==? J ("b", 1)

      isImbalanced "a"
        (  "a" =: (1, ["b","c","d"])
        <> "b" =: (1, [])
        <> "c" =: (1, [])
        <> "d" =: (1, ["e"])
        <> "e" =: (1, [])
        ) ==? J ("d", 2)


      isImbalanced "a"
        (  "a" =: (1, [])
        ) ==? N

      isImbalanced "a"
        (  "a" =: (1, ["b"])
        <> "b" =: (1, [])
        ) ==? N

      isImbalanced "a"
        (  "a" =: (1, ["b"])
        <> "b" =: (1, ["e"])
        <> "e" =: (1, [])
        ) ==? N

      isImbalanced "a"
        (  "a" =: (1, ["b","c","d"])
        <> "b" =: (1, ["e"])
        <> "c" =: (1, ["f"])
        <> "d" =: (1, ["g"])
        <> "e" =: (1, [])
        <> "f" =: (1, [])
        <> "g" =: (1, [])
        ) ==? N

      isImbalanced "a"
        (  "a" =: (1, ["b","c","d"])
        <> "b" =: (1, ["g"])
        <> "c" =: (1, ["f"])
        <> "d" =: (1, ["e"])
        <> "e" =: (1, [])
        <> "f" =: (1, [])
        <> "g" =: (2, [])
        ) ==? J ("b", 3)

      isImbalanced "a"
        (  "a" =: (1, ["b","c","d"])
        <> "b" =: (2, ["e"])
        <> "c" =: (2, ["f"])
        <> "d" =: (1, ["g"])
        <> "e" =: (1, [])
        <> "f" =: (1, [])
        <> "g" =: (2, [])
        ) ==? N

      isImbalanced "a"
        (  "a" =: (1, ["b","c","d"])
        <> "b" =: (2, ["e"])
        <> "c" =: (2, ["f"])
        <> "d" =: (1, ["g", "h"])
        <> "e" =: (1, [])
        <> "f" =: (1, [])
        <> "g" =: (1, [])
        <> "h" =: (1, [])
        ) ==? N

      isImbalanced "a"
        (  "a" =: (1, ["b","c","d"])
        <> "b" =: (3, ["e"])
        <> "c" =: (3, ["f"])
        <> "d" =: (1, ["g", "h"])
        <> "e" =: (1, [])
        <> "f" =: (1, [])
        <> "g" =: (2, [])
        <> "h" =: (1, [])
        ) ==? J ("h", 1)

      -- ugml + (gyxo + ebii + jptl) = 68 + (61 + 61 + 61) = 251
      -- padx + (pbga + havc + goyq) = 45 + (66 + 66 + 66) = 243
      -- fwft + (ktlj + cntj + xhth) = 72 + (57 + 57 + 57) = 243

    specify "myParseLine" $ do
      myParseLine "asd (12)" ==? ("asd" =: (12, []))
      myParseLine "ddd (13) -> fff" ==? ("ddd" =: (13, ["fff"]))
      myParseLine "ddd (13) -> fff, sss" ==? ("ddd" =: (13, ["fff", "sss"]))

    specify "part 1x" $ do
      unique [1] ==? [1]
      unique [1, 1] ==? []
    specify "part 1x" $ do
      uniqueBy fst [(1, 10)] ==? [(1, 10)]
      uniqueBy fst [(1, 10), (1, 11)] ==? []

    specify "part 2" $ do
      pending


  -- safePrint (53046 - 9)
  testInp $> cs $> T.lines $> map myParseLine $> mconcat
    $> weight "nmhmw"
    $> safePrint
  testInp $> cs $> T.lines $> map myParseLine $> mconcat
    $> weight "pknpuej"
    $> safePrint
  testInp $> cs $> T.lines $> map myParseLine $> mconcat
    $> weight "rfkvap"
    $> safePrint
  testInp $> cs $> T.lines $> map myParseLine $> mconcat
    $> isImbalanced "azqje"
    $> safePrint

type State7 = M.Map Label (SelfWeight, [Label])
type Label = Text
type SelfWeight = Weight
type Weight = Int

myParseLine :: Text -> State7
myParseLine str = name =: (selfW, parents)
  where
  name = fromJust $ matchedText $ str ?=~ [re|^([a-z]+)|]
  selfW = read $ cs $ str ?=~/ [ed|.*\(([0-9]+)\).*///$1|]
  parents = L.filter (/="") $ map (cs.>L.filter (isAlpha).>cs)
    $ T.words $ fromMaybe "" $ matchedText $ str ?=~ [re| -> .*$|]


sg :: Ord a => [a] -> [[a]]
sg = L.group . L.sort

sgBy :: Ord b => (a -> b) -> [a] -> [[a]]
sgBy fn = L.groupBy (\a b -> fn a == fn b) . L.sortOn fn

filterByLength :: Ord a => (Int -> Bool) -> [a] -> [[a]]
filterByLength p = L.filter (p . length) . sg

filterByLengthBy :: Ord b => (a -> b) -> (Int -> Bool) -> [a] -> [[a]]
filterByLengthBy fn p = L.filter (p . length) . sgBy fn


isImbalanced :: Label -> State7 -> Maybe (Label, Weight)
isImbalanced s m = map (flip isImbalanced m) parents $> L.find isJust $> join  $> \case
  J a -> J a
  N   -> map phi parents $> zeta $> uniqueBy snd $> (\case (a:_) -> J a; [] -> N)
  where
  (selfWeight, parents) = M.lookup s m $> fromMaybe (0, [])
  phi p = (p, weight p m)
  zeta [_] = []
  zeta a   = a

uniqueBy :: Ord b => (a -> b) -> [a] -> [a]
uniqueBy fn = concat . filterByLengthBy fn (==1)


weight :: Label -> State7 -> Weight
weight s m = selfWeight + sum (L.map (flip weight m) parents)
  where
  (selfWeight, parents) = M.lookup s m $> fromMaybe (0, [])


pattern J a = Just a
pattern N   = Nothing

iterMayN advance n s = iterate (>>= advance) (Just s) !! n
iterN advance n s = iterate advance s !! n
fl = zip [0..] .> fromList

noop :: Monad m => m ()
noop = return ()

sub = subtract

safePrint = id .> show .> take 1000 .> putStrLn

(=:) = M.singleton

type HCS = HasCallStack
(==?) :: (HCS, _) => _
(==?) = shouldBe
